/*This source code copyrighted by Lazy Foo' Productions (2004-2015)
and may not be redistributed without written permission.*/

//Using SDL and standard IO
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mixer.h>

#include <stdio.h>
#include <iostream>
#include <string>
#include <ctype.h>


//Screen dimension constants
const int SCREEN_WIDTH = 640;
const int SCREEN_HEIGHT = 470;

SDL_Window* gWindow = NULL; // The window
SDL_Surface* gScreenSurface = NULL; // The surface of the window
SDL_Surface* gHelloWorld = NULL; //the image to be put on it


bool init()
{
	bool success = true;


	//Init SDL
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) < 0)
	{
		printf("SDL could not initialize! SDL Error: %s\n", SDL_GetError());
		success = false;
	}
	else
	{
		//Create the window
		gWindow = SDL_CreateWindow("SDL Tutorial 2", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
		if (gWindow == NULL)
		{
			//Window failed to be created
			printf("Window could not be created. SDL_Error: %s\n", SDL_GetError());
			success = false;
		}
		else
		{
			//Get the windows surface into global
			gScreenSurface = SDL_GetWindowSurface(gWindow);
		}

		//Initialize SDL_mixer
		if (Mix_OpenAudio(44100, MIX_DEFAULT_FORMAT, 2, 2048) < 0)
		{
			printf("SDL_mixer could not initialize! SDL_mixer Error: %s\n", Mix_GetError());
			success = false;
		}

	}
	return success;
}

bool loadMedia()
{
	bool success = true;
	
	return success;
}

void close()
{
	// Deallocate and Destroy

	SDL_DestroyWindow(gWindow);
	gWindow = NULL;

	Mix_Quit();
	IMG_Quit();
	SDL_Quit();
}


int main(int argc, char* args[])
{
	bool quit = false;
	SDL_Event e;
	char temp;
	char numberKeys[10] = { '-', '-', '-', '-', '-', '-', '-', '-', '-', '-' };
	bool changed = true;


	if (!init())
	{
		printf("Failed to init.\n");
	}
	else
	{
		if (!loadMedia())
		{
			printf("Failed to load media.\n");
		}
		else
		{
			while (!quit)
			{
				//Handle events on queue
				while (SDL_PollEvent(&e) != 0)
				{
					//User requests quit
					if (e.type == SDL_QUIT)
					{
						quit = true;
					}

					switch (e.type)
					{

					case SDL_KEYUP:
						temp = (char)e.key.keysym.sym;

						if (isalpha(temp))
						{
							std::cout << temp << " is now up\n";
						}

						if (isalnum(temp))
						{
							int value = temp - '0';
							numberKeys[value] = '-';
							changed = true;
						}

						break;

					case SDL_KEYDOWN:	
						temp = (char)e.key.keysym.sym;
						
						if (isalpha(temp))
						{
							std::cout << temp << " is down\n";
						}

						if (isalnum(temp))
						{
							int value = temp - '0';
							numberKeys[value] = '+';
							changed = true;
						}

					default:
						break;
					}


				}

				if (changed)
				{
					
					for (int i = 0; i < sizeof(numberKeys); i++)
					{
						std::cout << numberKeys[i];
					}
					std::cout << std::endl;
					changed = false;
				}
				SDL_UpdateWindowSurface(gWindow);

			}
		}

	}

	close();

	return 0;
}